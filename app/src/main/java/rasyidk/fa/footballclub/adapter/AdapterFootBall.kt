package rasyidk.fa.footballclub.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import org.jetbrains.anko.AnkoContext
import rasyidk.fa.footballclub.R
import rasyidk.fa.footballclub.anko_layout.ItemView
import rasyidk.fa.footballclub.model.Item
import kotlinx.android.synthetic.main.item_card.view.*

class AdapterFootBall(private val items: List<Item>, private val listener: (Item) -> Unit):
        RecyclerView.Adapter<AdapterFootBall.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterFootBall.ViewHolder {
        return ViewHolder(ItemView().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: AdapterFootBall.ViewHolder, position: Int) {
        holder.bindItem(items[position], listener)

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){

        private val name = view.txtNama
        private val image = view.findViewById<ImageView>(R.id.imgFootBall)

        fun bindItem(items: Item, listener: (Item) -> Unit) {
            name.text = items.name
            Glide.with(itemView.context).load(items.image).into(image)
            itemView.setOnClickListener {
                listener(items)
            }
        }
    }
}