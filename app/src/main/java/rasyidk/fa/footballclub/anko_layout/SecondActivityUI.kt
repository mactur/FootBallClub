package rasyidk.fa.footballclub.anko_layout

import android.graphics.Typeface
import android.view.Gravity
import android.widget.LinearLayout
import org.jetbrains.anko.*
import rasyidk.fa.footballclub.R
import rasyidk.fa.footballclub.SecondActivity

class SecondActivityUI : AnkoComponent<SecondActivity> {

    override fun createView(ui: AnkoContext<SecondActivity>) = with(ui) {
        verticalLayout {
            lparams(width = matchParent, height = wrapContent)
            padding = dip(10)
            orientation = LinearLayout.VERTICAL

            imageView {
                id = R.id.imgDtlFootBall

            }.lparams(width = dip(80), height = dip(80)) {
                gravity = Gravity.CENTER_HORIZONTAL
            }

            textView {
                id = R.id.txtDtlNama
                textSize = 25f
                typeface = Typeface.DEFAULT_BOLD
                gravity = Gravity.CENTER_HORIZONTAL

            }.lparams(width = matchParent, height = wrapContent) {

                bottomMargin = dip(15)
            }

            textView {
                id = R.id.txtDtlDesc
                textSize = 15f

            }.lparams(width = matchParent, height = wrapContent) {
                gravity = Gravity.CENTER_HORIZONTAL
            }
        }
    }
}