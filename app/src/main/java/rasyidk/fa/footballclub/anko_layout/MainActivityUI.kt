package rasyidk.fa.footballclub.anko_layout

import android.support.v7.widget.LinearLayoutManager
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import rasyidk.fa.footballclub.MainActivity
import rasyidk.fa.footballclub.SecondActivity
import rasyidk.fa.footballclub.adapter.AdapterFootBall
import rasyidk.fa.footballclub.model.Item

class MainActivityUI(private val items: MutableList<Item>) : AnkoComponent<MainActivity> {
    private lateinit var adapterFootBall: AdapterFootBall

    override fun createView(ui: AnkoContext<MainActivity>) = with(ui) {
        adapterFootBall = AdapterFootBall(items) {
            startActivity<SecondActivity>("img" to it.image, "name" to it.name, "desc" to it.desc)
        }

        relativeLayout {
            lparams(width = matchParent, height = matchParent)

            recyclerView {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, true)
                adapter = adapterFootBall
            }.lparams(width = matchParent, height = wrapContent) {
                alignParentTop()
            }
        }
    }
}