package rasyidk.fa.footballclub

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import org.jetbrains.anko.*
import rasyidk.fa.footballclub.anko_layout.MainActivityUI
import rasyidk.fa.footballclub.model.Item

class MainActivity : AppCompatActivity() {

    private var items: MutableList<Item> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initData()

        MainActivityUI(items).setContentView(this)
    }

    private fun initData(){
        val name = resources.getStringArray(R.array.club_name)
        val desc = resources.getStringArray(R.array.club_desc)
        val image = resources.obtainTypedArray(R.array.club_image)
        items.clear()
        for (i in name.indices) {
            items.add(Item(name[i], desc[i],
                    image.getResourceId(i, 0)))
        }

        image.recycle()
    }
}
