package rasyidk.fa.footballclub.model

data class Item (val name: String?, val desc: String?, val image: Int)